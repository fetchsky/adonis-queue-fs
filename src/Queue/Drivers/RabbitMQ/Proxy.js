"use strict";

/**
 * @typedef {import('./index')} Driver
 */
/**
 * @typedef {import('../../Client')} Client
 */

const _ = require("lodash");
const amqplib = require("amqplib");
const amqplibDelay = require("amqp-delay.node");

class RabbitMQDriverProxy {
  constructor(driver, client) {
    /**
     * @type {Driver}
     */
    this.__driver = new Proxy(driver, this);
    /**
     * @type {Client}
     */
    this.__client = client;
  }

  get(driver, prop) {
    if (!prop || typeof prop !== "string" || prop.indexOf("__") === 0) {
      return;
    }
    if (prop.indexOf("_") < 0 && typeof driver[prop] !== "undefined") {
      if (typeof driver[prop] === "function") {
        return driver[prop].bind(this.__driver);
      }
      return driver[prop];
    }
    if (typeof this[prop] !== "undefined") {
      if (typeof this[prop] === "function") {
        return this[prop].bind(this);
      }
      return this[prop];
    }
  }

  /**
   * This method is used to validate queue config
   */
  _validateConfig() {
    if (!this.__client.getConfig().connectionString) {
      throw Error("Connection string is not specified in queue config");
    }
  }

  /**
   * This method is used to connect client to server
   */
  async _connect() {
    let config = this.__client.getConfig();
    this.__connection = await amqplib.connect(config.connectionString);
    this.__connection.on("close", async () => {
      this.__client
        .getLogger()
        .error(
          `${new Date()} ${config.connection} queue connection is disconnected`
        );
      await this.__client.reconnect();
    });
    this.__connection.on("error", async () => {
      this.__client
        .getLogger()
        .error(
          `${new Date()} ${
            config.connection
          } queue connection has encountered an error`
        );
      await this.__client.reconnect();
    });
    this.__channels = {
      publisher: null,
      consumer: {}
    };
    let channel = await this.__connection.createChannel();
    for (let key in config.queues) {
      let queue = this.__getQueue(key);
      await channel.assertQueue(queue, {
        durable: true
      });
      this.__channels.publisher = channel;
    }
  }

  /**
   * This method is used to transform outgoing message
   *
   * @param {String} msg
   */
  async _transformOutgoingMessage(msg) {
    return Buffer.from(msg);
  }

  /**
   * This method is used to push msg to queue
   *
   * @param {String} queueKey
   * @param {Buffer} msg
   * @param {Number} [delay]
   */
  async _push(queueKey, msg, delay) {
    if (typeof delay === "number" && delay > 0) {
      return this.__wait(delay).then(() => {
        return this.__push(queueKey, msg);
      });
    }
    return this.__push(queueKey, msg);
  }

  /**
   * This method is used to consume messages from queue channel
   *
   * @param {String} queueKey
   * @param {Function} handle
   */
  async _consume(queueKey, handle) {
    let queue = this.__getQueue(queueKey);
    let channel = await this.__getConsumingChannel(queueKey);
    channel.consume(queue, handle);
  }

  /**
   * This method is used to transform incoming message
   *
   * @param {Object} msg
   */
  async _transformIncomingMessage(msg) {
    return msg.content.toString();
  }

  /**
   * This method is used to send ack for msg to queue channel.
   *
   * @param {String} queueKey
   * @param {Object} msg
   */
  async _sendAck(queueKey, msg) {
    let channel = await this.__getConsumingChannel(queueKey);
    return channel.ack(msg);
  }

  /**
   * This method is used to push msg to queue
   *
   * @param {String} queueKey
   * @param {Buffer} msg
   */
  async __push(queueKey, msg) {
    let queue = this.__getQueue(queueKey);
    const channel = this.__getPublishingChannel();
    return channel.sendToQueue(queue, msg, { persistent: true });
  }

  /**
   * This method is used to introduce delay.
   *
   * @param {Number} timeout
   * @returns {Promise}
   */
  __wait(timeout) {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  /**
   * This method is used to get queue by key
   *
   * @param {String} queueKey
   * @returns {String}
   */
  __getQueue(queueKey) {
    let queue = this.__client.getConfig().queues[queueKey];
    if (!queue) {
      throw new Error(
        "Missing queue {" +
          queueKey +
          "}. Make sure you define it inside config/queue.js file"
      );
    }
    return queue;
  }

  /**
   * This method is used to get publishing channel for queue
   *
   * @returns {Object}
   */
  __getPublishingChannel() {
    return this.__channels.publisher;
  }

  /**
   * This method is used to get consuming channel for queue
   *
   * @param {String} queueKey
   */
  async __getConsumingChannel(queueKey) {
    let channel = this.__channels.consumer[queueKey];
    if (!channel) {
      channel = await this.__connection.createChannel();
      let queue = this.__getQueue(queueKey);
      await channel.assertQueue(queue, {
        durable: true
      });
      await channel.prefetch(1);
      this.__channels.consumer[queueKey] = channel;
    }
    return channel;
  }
}

module.exports = RabbitMQDriverProxy;
