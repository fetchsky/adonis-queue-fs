"use strict";

const amqplib = require("amqplib");
const RabbitMQProxy = require("./Proxy");
const QueueDriver = require("../../BaseDriver");

class RabbitMQDriver extends QueueDriver {
  constructor(client) {
    super(client);
    let proxy = new RabbitMQProxy(this, client);
    return new Proxy(this, proxy);
  }
  /**
   * This method is used to validate queue config
   */
  validateConfig() {
    return this._validateConfig();
  }

  /**
   * This method is used to connect client to server
   */
  async connect() {
    return this._connect();
  }

  /**
   * This method is used to transform outgoing message
   *
   * @param {*} msg
   */
  async transformOutgoingMessage(msg) {
    return this._transformOutgoingMessage(msg);
  }

  /**
   * This method is used to push msg to queue
   *
   * @param {String} queueKey
   * @param {Buffer} msg
   * @param {Number} [delay]
   */
  async push(queueKey, msg, delay) {
    return this._push(queueKey, msg, delay);
  }

  /**
   * This method is used to consume messages from queue channel
   *
   * @param {String} queueKey
   * @param {Function} handle
   */
  async consume(queueKey, handle) {
    return this._consume(queueKey, handle);
  }

  /**
   * This method is used to transform incoming message
   *
   * @param {String} msg
   */
  async transformIncomingMessage(msg) {
    return this._transformIncomingMessage(msg);
  }

  /**
   * This method is used to send ack for msg to queue channel.
   *
   * @param {String} queueKey
   * @param {Object} msg
   */
  async sendAck(queueKey, msg) {
    return this._sendAck(queueKey, msg);
  }
}

module.exports = RabbitMQDriver;
