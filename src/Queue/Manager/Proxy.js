"use strict";

/**
 * @typedef {import('./index')} QueueManager
 */
const Client = require("../Client");

class QueueManagerProxy {
  /**
   * Creates a new instance
   */
  constructor(manager, Config) {
    /**
     * @type {QueueManager}
     */
    this.__manager = new Proxy(manager, this);
    this._clients = {};
    let queueConfig = Config.get("queue");
    for (let connection in queueConfig.connections) {
      let config = queueConfig.connections[connection];
      config.connection = connection;
      this._clients[connection] = new Client(config);
    }
    if (queueConfig.connection) {
      this._defaultClient = this._clients[queueConfig.connection];
    }
  }

  get(manager, prop) {
    if (!prop || typeof prop !== "string" || prop.indexOf("__") === 0) {
      return;
    }
    if (prop.indexOf("_") < 0 && typeof manager[prop] !== "undefined") {
      if (typeof manager[prop] === "function") {
        return manager[prop].bind(this.__manager);
      }
      return manager[prop];
    }
    if (typeof this[prop] !== "undefined") {
      if (typeof this[prop] === "function") {
        return this[prop].bind(this);
      }
      return this[prop];
    }
    if (typeof this._defaultClient[prop] !== "undefined") {
      if (typeof this._defaultClient[prop] === "function") {
        return this._defaultClient[prop].bind(this._defaultClient);
      }
      return this._defaultClient[prop];
    }
    if (typeof Client.prototype[prop] !== "undefined") {
      throw Error("Please select connection first");
    }
  }

  /**
   * Returns client for provided connection
   *
   * @param {String} name
   * @returns {Client}
   */
  _connection(name) {
    if (typeof name === "string" && name) {
      let client = this._clients[name];
      if (client) {
        return client;
      }
      throw Error("Connection name is invalid");
    }
    throw Error("Connection name is not specified");
  }

  /**
   * This method is used to connect and monitor connections.
   *
   * @returns {Promise}
   */
  async _createConnections() {
    for (let connection in this._clients) {
      /**
       * @type {import('../Client')}
       */
      let client = this._clients[connection];
      await client.connect();
    }
  }
}
module.exports = QueueManagerProxy;
