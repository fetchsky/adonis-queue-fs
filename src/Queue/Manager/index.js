"use strict";

const QueueManagerProxy = require("./Proxy");

class QueueManager {
  /**
   * Creates a new instance
   */
  constructor(Config) {
    let proxy = new QueueManagerProxy(this, Config);
    return new Proxy(this, proxy);
  }

  /**
   * Returns client for provided connection
   *
   * @param {String} name
   * @returns {import('../Client')}
   */
  connection(name) {
    return this._connection(name);
  }

  /**
   * This method is used to connect and monitor connections.
   *
   * @returns {Promise}
   */
  async createConnections() {
    return this._createConnections();
  }
}
module.exports = QueueManager;
