"use strict";

/**
 * @typedef {import('./index')} Client
 */

const _ = require("lodash");
const promisify = require("promisify-node");
const drivers = {
  rabbitmq: require("../Drivers/RabbitMQ")
};

class ClientProxy {
  constructor(client, config) {
    /**
     * @type {Client}
     */
    this.__client = new Proxy(client, this);
    this._isConnected = false;
    this.__events = {
      beforeConnect: {},
      afterConnect: {},
      messagePush: {},
      messageReceive: {},
      beforeConsume: {},
      messageConsumptionFail: {}
    };
    this._config = configureOptions(config);
    this._logger = configureLogger(this._config);
    /**
     * @type {import('../BaseDriver')}
     */
    this.__driver = configureDriver(this.__client);
  }

  get(client, prop) {
    if (!prop || typeof prop !== "string" || prop.indexOf("__") === 0) {
      return;
    }
    if (prop.indexOf("_") < 0 && typeof client[prop] !== "undefined") {
      if (typeof client[prop] === "function") {
        return client[prop].bind(this.__client);
      }
      return client[prop];
    }
    if (typeof this[prop] !== "undefined") {
      if (typeof this[prop] === "function") {
        return this[prop].bind(this);
      }
      return this[prop];
    }
  }

  /**
   * This method is used to add event listener
   *
   * @param {String} event
   * @param {Function} listener
   */
  _on(event, listener) {
    if (!event || typeof event !== "string") {
      throw Error("Please provide a valid event name");
    }
    let eventListeners = this.__events[event];
    if (!eventListeners) {
      throw Error("This event is not supported");
    }
    if (typeof listener === "function") {
      let uniqId;
      while (!uniqId || eventListeners[uniqId]) {
        uniqId = _.uniqueId("ref_");
      }
      try {
        eventListeners[uniqId] = promisify(listener);
      } catch (err) {}
      return uniqId;
    }
  }

  /**
   * This method is used to remove event listener
   *
   * @param {String} event
   * @param {String} eventId
   */
  _removeListener(event, eventId) {
    if (!event || !eventId || typeof event !== "string") {
      throw Error("Please provide a valid event name & id");
    }
    let eventListeners = this.__events[event];
    if (!eventListeners) {
      throw Error("This event is not supported");
    }
    if (eventListeners[eventId]) {
      delete eventListeners[eventId];
    }
  }

  /**
   * This method will be executed before connecting to queue server
   */
  async __beforeConnect() {
    try {
      await this.__callEventListeners("beforeConnect", {
        connection: this._config.connection
      });
    } catch (error) {
      this._logger.error(error);
    }
  }

  /**
   * This method is used to create connection between queue client and server
   */
  async _connect() {
    await this.__beforeConnect();
    while (!this._isConnected) {
      try {
        await this.__driver.connect();
        this._isConnected = true;
        this._logger.info(
          `${new Date()} ${
            this._config.connection
          } queue connection established successfully`
        );
        await this.__afterConnect();
      } catch (err) {
        this._logger.error(
          `${new Date()} Failed to establish ${
            this._config.connection
          } queue connection`,
          err
        );
        await this.__wait(this._config.reconnectInterval);
      }
    }
  }

  /**
   * This method will be executed once client gets connected with queue server
   */
  async __afterConnect() {
    try {
      await this.__callEventListeners("afterConnect", {
        connection: this._config.connection
      });
    } catch (error) {
      this._logger.error(error);
    }
  }

  /**
   * This method is used to reconnect client to queue server
   */
  async _reconnect() {
    this._isConnected = false;
    await this.__wait(this._config.reconnectInterval);
    this._connect();
  }

  /**
   * This method is used to push msg to queue
   *
   * @param {String} queueKey
   * @param {*} msg
   * @param {Number} [delay]
   */
  async _push(queueKey, msg, delay) {
    if (!this._isConnected) {
      await this._connect();
    }
    msg = JSON.stringify(msg);
    if (typeof this.__driver.transformOutgoingMessage === "function") {
      let transformedMsg = await this.__driver.transformOutgoingMessage(msg);
      await this.__driver.push(queueKey, transformedMsg, delay);
    } else {
      await this.__driver.push(queueKey, msg, delay);
    }
    try {
      await this.__callEventListeners("messagePush", {
        connection: this._config.connection,
        queueKey,
        content: msg
      });
    } catch (error) {
      this._logger.error(error);
    }
  }

  /**
   * This method is used to consume messages from queue channel
   *
   * @param {String} queueKey
   * @param {Function} [listener]
   */
  async _consume(queueKey, listener) {
    if (!this._isConnected) {
      await this._connect();
    }
    await this.__callEventListeners("beforeConsume", {
      connection: this._config.connection,
      queueKey
    });
    return this.__driver.consume(queueKey, async (msg) => {
      this._logger.info(`${new Date()} Received item from queue.`);
      let transformedMsg = await this.__transformIncomingMessage(queueKey, msg);
      if (!transformedMsg.status) {
        return;
      }
      this._logger.debug(
        "Queue item: " + JSON.stringify(transformedMsg.content)
      );
      try {
        await this.__callMessageListeners(
          queueKey,
          transformedMsg.content,
          listener
        );
        await this.__driver.sendAck(queueKey, msg);
        this._logger.info(`${new Date()} Sending acknowledgment...`);
      } catch (error) {
        this._logger.error(error);
        this.__callEventListeners("messageConsumptionFail", {
          connection: this._config.connection,
          queueKey,
          content: transformedMsg.content,
          error
        });
      }
    });
  }

  /**
   * This method is used to introduce delay.
   *
   * @param {Number} timeout
   * @returns {Promise}
   */
  __wait(timeout) {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  }

  /**
   * This method is used to call event listeners
   *
   * @param {String} event
   * @param {*} [eventData]
   */
  __callEventListeners(event, eventData) {
    let promises = [];
    for (let eventId in this.__events[event]) {
      let listener = this.__events[event][eventId];
      if (typeof listener === "function") {
        promises.push(listener.call(null, eventData));
      }
    }
    return Promise.all(promises);
  }

  /**
   *
   * @param {String} queueKey
   * @param {*} msg
   * @param {Function} [priorityListener]
   */
  __callMessageListeners(queueKey, msg, priorityListener) {
    let promises = [];
    if (typeof priorityListener === "function") {
      let promisingHandler = promisify(priorityListener);
      promises.push(promisingHandler.call(null, msg));
    }
    promises.push(
      this.__callEventListeners("messageReceive", {
        connection: this._config.connection,
        queueKey,
        content: msg
      })
    );
    return Promise.all(promises);
  }

  /**
   * This method is used to transform incoming message
   *
   * @param {String} queueKey
   * @param {Object} msg
   */
  async __transformIncomingMessage(queueKey, msg) {
    let content = msg;
    try {
      if (typeof this.__driver.transformIncomingMessage === "function") {
        content = await this.__driver.transformIncomingMessage(msg);
      }
      try {
        content = JSON.parse(content);
      } catch (error) {}
      return { status: true, content };
    } catch (error) {
      this._logger.error(error);
      this.__callEventListeners("messageConsumptionFail", {
        connection: this._config.connection,
        queueKey,
        content: msg,
        error
      });
      return { status: false };
    }
  }
}

function configureDriver(client) {
  let driver;
  let driverName = client.getConfig().driver;
  if (driverName && typeof driverName === "string") {
    if (Object.keys(drivers).indexOf(driverName) >= 0) {
      driver = new drivers[driverName](client);
    } else {
      try {
        driver = new use(driverName)(client);
      } catch (error) {
        throw Error("Invalid queue driver");
      }
    }
  } else {
    throw Error("Queue driver not found");
  }
  if (typeof driver.validateConfig === "function") {
    driver.validateConfig();
  }
  return driver;
}

function configureLogger(config) {
  let logger;
  if (config.logger && typeof config.logger === "string") {
    logger = use(config.logger);
    try {
    } catch (error) {
      throw Error("Logger not found");
    }
  } else {
    logger = use("Logger");
  }
  return logger;
}

function configureOptions(config) {
  let options = {
    ...config,
    queues: {},
    reconnectInterval: parseInt(config.reconnectInterval)
  };
  if (isNaN(options.reconnectInterval)) {
    options.reconnectInterval = 30000;
  }
  for (let key in config.queues) {
    if (config.queues[key] && typeof config.queues[key] === "string") {
      options.queues[key] = config.queues[key];
    }
  }
  return options;
}

module.exports = ClientProxy;
