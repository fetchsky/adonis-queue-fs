"use strict";

const ClientProxy = require("./Proxy");

class Client {
  constructor(config) {
    let proxy = new ClientProxy(this, config);
    return new Proxy(this, proxy);
  }

  isConnected() {
    return this._isConnected;
  }

  getConfig() {
    return this._config;
  }

  getLogger() {
    return this._logger;
  }

  /**
   * This method is used to add event listener
   *
   * @param {String} event
   * @param {Function} listener
   */
  on(event, listener) {
    return this._on(event, listener);
  }

  /**
   * This method is used to remove event listener
   *
   * @param {String} event
   * @param {String} eventId
   */
  removeListener(event, eventId) {
    return this._removeListener(event, eventId);
  }

  /**
   * This method is used to create connection between queue client and server
   */
  async connect() {
    return this._connect();
  }

  /**
   * This method is used to reconnect client to queue server
   */
  async reconnect() {
    return this._reconnect();
  }

  /**
   * This method is used to push msg to queue
   *
   * @param {String} queueKey
   * @param {*} msg
   * @param {Number} [delay]
   */
  async push(queueKey, msg, delay) {
    return this._push(queueKey, msg, delay);
  }

  /**
   * This method is used to consume messages from queue channel
   *
   * @param {String} queueKey
   * @param {Function} handle
   */
  async consume(queueKey, handle) {
    return this._consume(queueKey, handle);
  }
}

module.exports = Client;
