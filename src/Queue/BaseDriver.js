"use strict";

class BaseDriver {
  constructor(client) {
    this.client = client;
  }

  /**
   * This method is used to validate queue config
   */
  validateConfig() {}

  /**
   * This method is used to create connection between queue client and server
   */
  async connect() {
    throw Error("Please define `connect` method in your driver class");
  }

  /**
   * This method is used to transform outgoing message
   *
   * @param {*} msg
   */
  async transformOutgoingMessage(msg) {
    return msg;
  }

  /**
   * This method is used to push msg to queue
   *
   * @param {String} queueKey
   * @param {*} msg
   * @param {Number} [delay]
   */
  async push(queueKey, msg, delay) {
    throw Error("Please define `push` method in your driver class");
  }

  /**
   * This method is used to consume messages from queue channel
   *
   * @param {String} queueKey
   * @param {Function} handle
   */
  async consume(queueKey, handle) {
    throw Error("Please define `consume` method in your driver class");
  }

  /**
   * This method is used to transform incoming message
   *
   * @param {Object} msg
   */
  async transformIncomingMessage(msg) {
    return msg;
  }

  /**
   * This method is used to send ack for msg to queue channel.
   *
   * @param {String} queueKey
   * @param {Object} msg
   */
  async sendAck(queueKey, msg) {
    throw Error("Please define `sendAck` method in your driver class");
  }
}

module.exports = BaseDriver;
