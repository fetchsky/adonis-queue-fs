"use strict";
/**
 * @typedef {import('.')} Job
 */
/**
 * @typedef {import('../Manager')} QueueManager
 */

class QueueJobProxy {
  constructor(Job, Config) {
    /**
     * @type {Job}
     */
    this.__Job = new Proxy(Job, this);
    /**
     * @type {QueueManager}
     */
    this.__path = "App/Jobs/" + Job.constructor.name;
    this.__QueueManager = use("FetchSky/Src/Queue");
    this.__Config = Config;
    this.__delay = 0;
    this.__queue = null;
    this.__connection = null;
    this.__isDispatched = false;
    this.__isDispatching = false;
  }

  get(job, prop) {
    if (!prop || typeof prop !== "string" || prop.indexOf("__") === 0) {
      return;
    }
    if (prop.indexOf("_") < 0 && typeof job[prop] !== "undefined") {
      if (typeof job[prop] === "function") {
        return job[prop].bind(this.__Job);
      }
      return job[prop];
    }
    if (typeof this[prop] !== "undefined") {
      if (typeof this[prop] === "function") {
        return this[prop].bind(this);
      }
      return this[prop];
    }
  }

  get path() {
    return this.__path;
  }

  /**
   * Returns the connection for queue
   */
  get _connection() {
    if (typeof this.__connection === "string" && this.__connection) {
      return this.__connection;
    }
    return this.__Config.get("queue.connection");
  }

  /**
   * Returns the desired queue for job
   */
  get _queue() {
    if (typeof this.__queue === "string" && this.__queue) {
      return this.__queue;
    }
    if (this._connection) {
      return this.__Config.get(
        "queue.connections." + this._connection + ".defaultQueue"
      );
    }
    return null;
  }

  /**
   * Returns if job is dispatchable or not
   */
  get _isDispatchable() {
    return !this.__isDispatching && !this.__isDispatched;
  }

  /**
   * Set the connection for queue
   *
   * @param {String} connection
   * @returns {Object}
   */
  _onConnection(connection) {
    let connections = Object.keys(this.__Config.get("queue.connections"));
    if (connection && connections.indexOf(connection) >= 0) {
      this.__connection = connection;
    } else if (connection) {
      throw Error(
        "Missing queue connection {" +
          connection +
          "}. Make sure you define it inside config/queue.js file"
      );
    } else {
      this.__connection = null;
    }
    return this.__Job;
  }

  /**
   * Set the desired queue for the job.
   *
   * @param {String} queueKey
   * @returns {Object}
   */
  _onQueue(queueKey) {
    if (queueKey && typeof queueKey === "string") {
      this.__queue = queueKey;
    } else if (queueKey) {
      throw Error(
        "Missing queue {" +
          queueKey +
          "}. Make sure you define it inside config/queue.js file"
      );
    } else {
      this.__queue = null;
    }
    return this.__Job;
  }

  /**
   * Adds delay to job dispatchment
   *
   * @param {Number} time
   */
  _delay(time) {
    if (typeof time === "number") {
      this.__delay = time;
    } else {
      this.__delay = 0;
    }
    return this.__Job;
  }

  /**
   * This method is used to dispatch job to respective queue
   *
   * @param data
   * @returns {Promise}
   */
  async _dispatch(data) {
    if (!this._isDispatchable) {
      throw Error("Job is already dispatched or being dispatched");
    }
    if (!this._queue) {
      throw Error("Missing default queue");
    }
    this.__isDispatching = true;
    var queueData = { data };
    if (this.__Job.path) {
      queueData.job = this.__Job.path;
    }
    try {
      await this.__QueueManager
        .connection(this._connection)
        .push(this._queue, queueData, this.__delay);
      this.__isDispatched = true;
    } catch (error) {
      throw error;
    } finally {
      this.__isDispatching = false;
    }
  }

  /**
   * This method is used to execute job
   *
   * @param data
   */
  async _dispatchNow(data) {
    if (!this._isDispatchable) {
      throw Error("Job is already dispatched or being dispatched");
    }
    this.__isDispatched = true;
    return this.__handle(data);
  }

  /**
   * Execute handler for the job.
   *
   * @param data
   * @returns {Promise}
   */
  async __handle(data) {
    if (typeof this.__Job.handle === "function") {
      return this.__Job.handle.call(this.__Job, data);
    }
  }
}

module.exports = QueueJobProxy;
