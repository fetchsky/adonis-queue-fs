"use strict";

// const Config = use("Config");
const QueueJobProxy = require("./Proxy");

class QueueJob {
  constructor(Config) {
    let proxy = new QueueJobProxy(this, Config);
    return new Proxy(this, proxy);
  }

  /**
   * Set the connection for queue
   *
   * @param {String} connection
   */
  static onConnection(connection) {
    let job = new this(use("Config"));
    return job.onConnection(connection);
  }

  /**
   * Set the desired queue for the job.
   *
   * @param {String} queue
   */
  static onQueue(queue) {
    let job = new this(use("Config"));
    return job.onQueue(queue);
  }

  /**
   * This method is used to dispatch job to default queue
   *
   * @param {*} data
   */
  static dispatch(data) {
    let job = new this(use("Config"));
    return job.dispatch(data);
  }

  /**
   * This method is used to dispatch job synchronous
   *
   * @param {*} data
   */
  static dispatchNow(data) {
    let job = new this(use("Config"));
    return job.dispatchNow(data);
  }

  /**
   * Set the connection for queue
   *
   * @param {String} connection
   * @returns {Object}
   */
  onConnection(connection) {
    return this._onConnection(connection);
  }

  /**
   * Set the desired queue for the job.
   *
   * @param {String} queue
   * @returns {Object}
   */
  onQueue(queue) {
    return this._onQueue(queue);
  }

  /**
   *
   * @param {Number} time
   */
  delay(time) {
    return this._delay(time);
  }

  /**
   * This method is used to dispatch job to respective queue
   *
   * @param data
   */
  async dispatch(data) {
    return this._dispatch(data);
  }

  /**
   * This method is used to execute job
   *
   * @param data
   */
  async dispatchNow(data) {
    return this._dispatchNow(data);
  }

  /**
   * Execution handler the job.
   *
   * @param data
   * @returns {Promise}
   */
  async handle(data) {
    return true;
  }
}

module.exports = QueueJob;
