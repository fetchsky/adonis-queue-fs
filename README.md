## Introduction

Queues allow you to defer the processing of a time consuming task, such as sending an email, until a later time. Deferring these time consuming tasks drastically speeds up web requests to your application.

If you haven't installed our package yet, please follow the [installation guide](instructions.md).

## Configuration

The queue configuration file is stored in `config/queue.php`.

## Dispatching Jobs

By default, all of the queueable jobs for your application are stored in the app/Jobs directory. If the app/Jobs directory doesn't exist, it will be created when you run the make:queue-job Ace command. You may generate a new queued job using the Adonis CLI:

`$ adonis make:queue-job ProcessPodcast`

### Class Structure

Job classes are very simple, normally containing only a handle method which is called when the job is processed by the queue. To get started, let's take a look at an example job class. In this example, we'll pretend we manage a podcast publishing service and need to process the uploaded podcast files before they are published:

```javascript
"use strict";

const QueueJob = use("FetchSky/Src/Queue/Job");

class ProcessPodcast extends QueueJob {
  /**
   * Path of the job.
   *
   * @returns {String}
   */
  get path() {
    return "App/Jobs/ProcessPodcast";
  }

  /**
   * Execute the job.
   *
   * @param {*} podcast
   */
  async handle(podcast) {
    // Process podcast
  }
}

module.exports = ProcessPodcast;
```

The handle method is called when the job is processed by the queue.

### Dispatching Jobs

Once you have written your job class, you may dispatch it using the dispatch method on the job itself. The arguments passed to the dispatch method will be given to the job's handle method:

```javascript
const Job = use("App/Jobs/ProcessPodcast");

Job.dispatch(podcast);
```

### Delayed Dispatching

If you would like to delay the dispatchment of a queued job, you may use the delay method when dispatching a job. For example, let's specify that a job should not be available in queue until 5 minutes after it has been dispatched:

```javascript
const Job = use("App/Jobs/ProcessPodcast");

Job.delay(300000).dispatch(podcast);
```

### Synchronous Dispatching

If you would like to dispatch a job immediately (synchronously), you may use the dispatchNow method. When using this method, the job will not be queued and will be run immediately within the current process:

```javascript
const Job = use("App/Jobs/ProcessPodcast");

Job.dispatchNow(podcast);
```

### Customizing The Queue & Connection

#### Dispatching To A Particular Queue

By pushing jobs to different queues, you may "categorize" your queued jobs and even prioritize how many workers you assign to various queues. Keep in mind, this does not push jobs to different queue "connections" as defined by your queue configuration file, but only to specific queues within a single connection. To specify the queue, use the onQueue method when dispatching the job:

```javascript
const Job = use("App/Jobs/ProcessPodcast");

Job.onQueue("PROCESSING").dispatch(data);
```

onQueue method accepts key of the queue which has been defined in `config/queue.js`

#### Dispatching To A Particular Connection

If you are working with multiple queue connections, you may specify which connection to push a job to. To specify the connection, use the onConnection method when dispatching the job:

```javascript
const Job = use("App/Jobs/ProcessPodcast");

Job.onConnection("primary").dispatch(data);
```

You may chain the onConnection and onQueue methods to specify the connection and the queue for a job:

```javascript
const Job = use("App/Jobs/ProcessPodcast");

Job.onConnection("primary")
  .onQueue("PROCESSING")
  .dispatch(data);
```

## Queuing Message Without Job

You can also push message to `DEFAULT` queue without job.

```javascript
const Queue = use("Queue");

Queue.connection("primary") // Optional
  .push("DEFAULT", message);
```

## Running The Queue Worker:

This package includes a queue worker that will process new jobs as they are pushed onto the queue. You may run the worker using the queue:work Ace command. Note that once the queue:work command has started, it will continue to run until it is manually stopped or you close your terminal:

`$ adonis queue:work`

Or

`$ adonis queue:work [connection] --queue=[queue]`

Remember, queue workers are long-lived processes and store the booted application state in memory. As a result, they will not notice changes in your code base after they have been started. So, during your deployment process, be sure to restart your queue workers.

## Listening Events

This package emits following events:

- beforeConnect
- afterConnect
- messagePush
- messageReceive
- beforeConsume
- messageConsumptionFail

You can listen any of the above event using:

```javascript
const Queue = use("Queue");

Queue.on(EVENT_NAME, args => {
  // Do your stuff
});
```

#### Listening Events From A Particular Connection

If you are working with multiple queue connections, you may specify connection for which you want to listen events:

```javascript
const Queue = use("Queue");

Queue.connection("primary").on(EVENT_NAME, args => {
  // Do your stuff
});
```

## Using Custom Queue Driver

Currently this plugin only supports RabbitMQ driver. But you can also add support for different drivers. To use your custom driver:

- Set `driver` value as path of your custom driver class in connection configuration
- Extend your driver class with `FetchSky/Src/Queue/Driver` module
