"use strict";

const { hooks } = use("@adonisjs/ignitor");
const QueueJob = require("../src/Queue/Job");
const Queue = require("../src/Queue/Manager");
const { ServiceProvider } = use("@adonisjs/fold");

class QueueProvider extends ServiceProvider {
  register() {
    this.app.singleton("FetchSky/Src/Queue", (app) => {
      return new Queue(app.use("Adonis/Src/Config"));
    });
    this.app.bind("FetchSky/Src/Queue/Job", () => {
      return QueueJob;
    });
    this.app.bind("FetchSky/Src/Queue/Driver", () => {
      return require("../src/Queue/BaseDriver");
    });
    this.app.bind("FetchSky/Commands/Queue:Job", () => {
      return require("../commands/Job");
    });
    this.app.bind("FetchSky/Commands/Queue:Work", () => {
      return require("../commands/Work");
    });
    this.app.bind("Queue", (app) => {
      return app.use("FetchSky/Src/Queue");
    });
    hooks.before.httpServer(() => {
      const QueueManager = use("FetchSky/Src/Queue");
      QueueManager.createConnections();
    });
  }

  boot() {
    const Helpers = use("Adonis/Src/Helpers");
    const queueFile = Helpers.appRoot("start/queue");
    let isQueueFileExists = false;
    try {
      require.resolve(queueFile);
      isQueueFileExists = true;
    } catch (err) {}
    if (isQueueFileExists) {
      require(queueFile);
    }
  }
}

module.exports = QueueProvider;
