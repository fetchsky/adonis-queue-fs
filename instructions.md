## Install plugin

`$ adonis install git+https://gitlab.com/fetchsky/adonis-queue-fs.git --as adonis-queue-fs`

## Register provider

Make sure to register this plugin's provider. The providers are registered inside `start/app.js`

```js
const providers = ["adonis-queue-fs/providers/QueueProvider"];
```

## Register commands

Register this plugin's commands. The commands are registered inside `start/app.js`

```js
const commands = [
  "FetchSky/Commands/Queue:Job",
  "FetchSky/Commands/Queue:Work",
];
```
