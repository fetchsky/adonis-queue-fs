"use strict";
const path = require("path");

module.exports = async function(cli) {
  try {
    await cli.makeConfig("queue.js", path.join(__dirname, "./templates/queue.mustache"));
    cli.command.completed("create", "config/queue.js");
  } catch (error) {
    // ignore errors
  }
};
