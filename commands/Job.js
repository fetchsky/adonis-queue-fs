"use strict";
const Drive = use("Drive");
const Helpers = use("Helpers");
const { startCase } = use("lodash");
const { render } = require("mustache");
const { Command } = use("@adonisjs/ace");

class QueueJob extends Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "make:queue-job {name: Unique name of job}";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Create a new queue based job";
  }

  /**
   * Execute the console command.
   *
   * @param {Object} args
   * @param {Object} options
   * @returns {Promise}
   */
  async handle(args, options) {
    if (!args.name) {
      this.error("Job name not specified");
      return false;
    }
    const fileContent = await Drive.disk("local").get(
      __dirname + "/../templates/job.mustache",
      "utf8"
    );
    let destFile =
      "app/Jobs/" +
      startCase(args.name.toLowerCase()).replace(/ /g, "") +
      ".js";
    let absoluteDestPath = Helpers.appRoot(destFile);
    let fileExists = await Drive.disk("local").exists(absoluteDestPath);
    if (!fileExists) {
      await Drive.disk("local").put(
        absoluteDestPath,
        render(fileContent, {
          Job: args.name
        })
      );
      this.completed("✔ create", destFile);
    } else {
      this.failed("Error", absoluteDestPath + " already exists");
    }
  }
}

module.exports = QueueJob;
