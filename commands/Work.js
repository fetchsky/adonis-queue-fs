"use strict";

const Event = use("Event");
const Config = use("Config");
const { Command } = use("@adonisjs/ace");
/**@type {import('../src/Queue/Manager')}*/
const QueueManager = use("FetchSky/Src/Queue");

class QueueWork extends Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "queue:work {connection?: Name of connection to be used} {--queue?=@value: Specify queue name} {--process?=@value: Specify process alias name}";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Starts a worker to consume queue messages";
  }

  /**
   * Log debug message on console window
   */
  debug(...args) {
    if (this.debugStatus) {
      console.log(this.chalk.yellow(...args));
    }
  }

  /**
   * Execute the console command.
   *
   * @param {Object} args
   * @param {Object} options
   * @returns {Promise}
   */
  async handle(args, options) {
    try {
      if (typeof options.process === "string") {
        process.title = options.process + "-" + process.pid;
      }
      this._connection = this._getConnection(args.connection);
      this._queueKey = this._getValidQueue(options.queue);
      this.debugStatus = Config.get(
        "queue.connections." + this._connection + ".debug",
        false
      );
      await QueueManager.connection(this._connection).consume(
        this._queueKey,
        this._executeJob.bind(this)
      );
    } catch (e) {
      this.error(e);
      process.exit(1);
    }
  }

  /**
   * Execute the respective job.
   *
   * @param {Object} data
   * @returns {Promise}
   */
  async _executeJob(data) {
    if (data && typeof data.job === "string") {
      try {
        const Job = use(data.job);
        const job = new Job(Config);
        await job.handle(data.data);
      } catch (error) {
        this.error(error);
        process.exit(1);
      }
    }
  }

  _getConnection(connection) {
    let connections = Object.keys(Config.get("queue.connections"));
    if (connection) {
      if (connections.indexOf(connection) >= 0) {
        return connection;
      }
      throw Error("Missing queue connection {" + connection + "}");
    }
    let defaultConnection = Config.get("queue.connection");
    if (defaultConnection && connections.indexOf(defaultConnection) >= 0) {
      return defaultConnection;
    }
    throw Error("Missing default connection for queue");
  }

  /**
   * Returns the desired queue for job
   *
   * @param {String} [queueKey]
   */
  _getValidQueue(queueKey) {
    let connectionConfig = Config.get("queue.connections." + this._connection);
    let queues = Object.keys(connectionConfig.queues || {});
    if (queueKey) {
      if (queues.indexOf(queueKey) >= 0) {
        return queueKey;
      }
      throw Error(
        "Missing queue {" +
          queueKey +
          "}. Make sure you define it inside config/queue.js file"
      );
    }
    let defaultQueue = connectionConfig.defaultQueue;
    if (defaultQueue) {
      if (queues.indexOf(defaultQueue) >= 0) {
        return defaultQueue;
      }
      throw Error(
        "Invalid default queue (key: " +
          defaultQueue +
          ") for connection (" +
          this._connection +
          ")"
      );
    }
    throw Error(
      "Missing default queue for connection (" + this._connection + ")"
    );
  }
}

module.exports = QueueWork;
